import Phaser from 'phaser'

export default class Level extends Phaser.Scene {
    constructor() {
        super({ key: 'level' })
    }

    create() {
        this.add.image(144, 256, 'background');

        this.bird = this.physics.add.sprite(144, 256, 'bird')

        this.anims.create({
            key: 'fly',
            frames: this.anims.generateFrameNumbers('bird', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        })

        this.bird.anims.play('fly', true)
        this.bird.setCollideWorldBounds(true)

        this.wing = this.sound.add('wing')

        this.input.on('pointerup', () => this.fly())
        this.fly()

        const config = {velocityX: -100, gravityY: -1200, immovable: true}
        this.pipes = this.physics.add.group(config)
        this.time.delayedCall(3000, this.createPipe, [], this)

        this.physics.add.collider(this.bird, this.pipes, () => this.die())
    }

    update() {
        const position = this.bird.body.position

        if (position.y > 480) {
            this.die()
        }
    }

    die() {
        this.scene.start('menu')
    }

    fly() {
        this.bird.setVelocityY(-500)

        this.sound.stopAll()
        this.wing.play()
    }

    createPipe() {
        const topPosition = Math.random() * (160 + 120) - 120
        const bottomPosition = topPosition + 440
        const topPipe = this.add.image(288, topPosition, 'pipe')
        topPipe.setFlipY(true)

        const bottomPipe = this.add.image(288, bottomPosition, 'pipe')

        this.pipes.add(topPipe)
        this.pipes.add(bottomPipe)

        this.time.delayedCall(3000, this.createPipe, [], this)
    }
}

