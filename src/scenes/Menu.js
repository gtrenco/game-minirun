import Phaser from 'phaser'

export default class Menu extends Phaser.Scene {
    constructor() {
        super({ key: 'menu' })
    }

    create() {
        this.add.image(144, 256, 'background');
        this.add.image(144, 200, 'logo');

        this.bird = this.add.sprite(144, 256, 'bird')

        this.anims.create({
            key: 'fly',
            frames: this.anims.generateFrameNumbers('bird', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        })

        this.bird.anims.play('fly', true)

        this.input.on('pointerup', () => this.scene.start('level'))
    }
}
