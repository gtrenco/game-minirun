import Phaser from 'phaser'
import background from '../assets/background-day.png'
import logo from '../assets/logo.png'
import bird from '../assets/bird.png'
import pipe from '../assets/pipe-green.png'
import wingOgg from '../assets/wing.ogg'
import wingWav from '../assets/wing.wav'

export default class Preload extends Phaser.Scene {
    preload() {
        const bg = this.add.rectangle(144, 256, 200, 40, 0x666666)
        const bar = this.add.rectangle(
            bg.x, bg.y, bg.width, bg.height, 0xffffff
        ).setScale(0, 1)

        this.load.image('background', background)
        this.load.image('logo', logo)
        const frameSize = { frameWidth: 34, frameHeight: 24 }
        this.load.spritesheet('bird', bird, frameSize)
        this.load.image('pipe', pipe)

        this.load.on('progress', progress => bar.setScale(progress, 1))

        this.load.audio('wing', [wingOgg, wingWav])
    }

    update() {
        this.scene.start('menu')
    }
}
