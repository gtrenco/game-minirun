import Phaser from 'phaser'
import space from '../assets/space3.png'
import logo from '../assets/phaser.png'

export default class FirstScene extends Phaser.Scene {
    preload() {
        this.load.image('space', space);
        this.load.image('logo', logo);
    }

    create() {
        this.add.image(400, 300, 'space');

        var logo = this.physics.add.image(400, 100, 'logo');

        logo.setVelocity(100, 200);
        logo.setBounce(1, 1);
        logo.setCollideWorldBounds(true);
    }
}
