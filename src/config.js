import Phaser from 'phaser'

import Preload from './scenes/Preload'
import Menu from './scenes/Menu'
import Level from './scenes/Level'

export default {
    type: Phaser.auto,
    title: 'Game',
    width: 288,
    height: 512,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1200 }
        }
    },
    scene: [
        Preload,
        Menu,
        Level,
    ]
}
